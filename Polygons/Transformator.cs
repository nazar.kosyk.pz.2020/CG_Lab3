﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Polygons
{
    public class Transformator
    {
        static public double[,] MultiplyMatrix(double[,] A, double[,] B)
        {
            int rA = A.GetLength(0);
            int cA = A.GetLength(1);
            int rB = B.GetLength(0);
            int cB = B.GetLength(1);

            if (cA != rB)
            {
                Console.WriteLine("Matrixes can't be multiplied!!");
                return null;
            }
            else
            {
                double temp = 0;
                double[,] kHasil = new double[rA, cB];

                for (int i = 0; i < rA; i++)
                {
                    for (int j = 0; j < cB; j++)
                    {
                        temp = 0;
                        for (int k = 0; k < cA; k++)
                        {
                            temp += A[i, k] * B[k, j];
                        }
                        kHasil[i, j] = temp;
                    }
                }

                return kHasil;
            }
        }

        static public double[,] MoveToVector(Point point, double x, double y)
        {
            var matrixA = new double[,] {
                { point.X },
                { point.Y },
                { 1}
            };

            var matrixB = new double[,] {
                { 1, 0, x },
                { 0, 1, y },
                { 0, 0, 1 },
            };
            return MultiplyMatrix(matrixB, matrixA);
        }

        static public double[,] ScaleTo(Point point, double kx, double ky)
        {
            var matrixA = new double[,] {
                { point.X },
                {point.Y },
                { 1}
            };

            var matrixB = new double[,] {
                { kx, 0, 0 },
                { 0, ky, 0 },
                { 0, 0, 1 },
            };
            return MultiplyMatrix(matrixB, matrixA);
        }

        static public double[,] MirrorXY(Point point)
        {
            var matrixA = new double[,] {
                { point.X },
                { point.Y }
            };

            var matrixB = new double[,] {
                { 0, -1 },
                { -1, 0 }
            };
            return MultiplyMatrix(matrixB, matrixA);
        }
    }
}
