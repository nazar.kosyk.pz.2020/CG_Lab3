﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Polygons
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //x*step
            var ellipse = new Ellipse();
            var diametr = 10;
            ellipse.Height = diametr;
            ellipse.Width = diametr;
            ellipse.Fill = Brushes.Red;
            ellipse.Stroke = Brushes.Black;
            ellipse.StrokeThickness = 1;
            PlotCanvas.Children.Add(ellipse);
            Canvas.SetZIndex(ellipse, 4);
            Canvas.SetTop(ellipse, PlotCanvas.Height / 2 - diametr / 2);
            Canvas.SetLeft(ellipse, PlotCanvas.Width / 2 - diametr / 2);

            var lineX = new Line();
            lineX.X1 = 0;
            lineX.Y1 = 0;
            lineX.X2 = PlotCanvas.Width;
            lineX.Y2 = 0;
            lineX.StrokeThickness = 2;
            lineX.Stroke = Brushes.Black;
            PlotCanvas.Children.Add(lineX);
            Canvas.SetZIndex(lineX, 3);
            Canvas.SetTop(lineX, PlotCanvas.Height / 2);

            var lineY = new Line();
            lineY.X1 = 0;
            lineY.Y1 = 0;
            lineY.X2 = 0;
            lineY.Y2 = PlotCanvas.Height;
            lineY.StrokeThickness = 2;
            lineY.Stroke = Brushes.Black;
            PlotCanvas.Children.Add(lineY);
            Canvas.SetZIndex(lineY, 3);
            Canvas.SetLeft(lineY, PlotCanvas.Width / 2);

            var gradation = 15;
            var step = PlotCanvas.Width / (2 * gradation);
            for (int i = 1; i < gradation * 2; i++)
            {
                var lineVertical = new Line();
                lineVertical.X1 = step * i;
                lineVertical.Y1 = 0;
                lineVertical.X2 = step * i;
                lineVertical.Y2 = PlotCanvas.Height;
                lineVertical.StrokeThickness = 0.5;
                lineVertical.Stroke = Brushes.Gray;
                PlotCanvas.Children.Add(lineVertical);
                Canvas.SetZIndex(lineVertical, 3);

                var lineHorisontal = new Line();
                lineHorisontal.X1 = 0;
                lineHorisontal.Y1 = step * i;
                lineHorisontal.X2 = PlotCanvas.Height;
                lineHorisontal.Y2 = step * i;
                lineHorisontal.StrokeThickness = 0.5;
                lineHorisontal.Stroke = Brushes.Gray;
                PlotCanvas.Children.Add(lineHorisontal);
                Canvas.SetZIndex(lineHorisontal, 3);
            }


            var triangle = TrianglePolygon;
            triangle.Points.Add(new Point(0, 0));
            triangle.Points.Add(new Point(3 * step, 0));
            triangle.Points.Add(new Point(0, -4 * step));
            triangle.Fill = Brushes.Yellow;
            triangle.Stroke = Brushes.Blue;
            triangle.StrokeThickness = 1;
            Canvas.SetZIndex(triangle, 5);
            Canvas.SetTop(triangle, PlotCanvas.Height / 2);
            Canvas.SetLeft(triangle, PlotCanvas.Width / 2);

            var triangleTransformator = new TriangleTransformator(triangle);
            triangle.Points = triangleTransformator.MoveTriangle(step * 2, -step * 2);
            triangle.Points = triangleTransformator.MoveTriangle(step * 1, -step * 2);
            //triangle.Points = triangleTransformator.ScaleTriangle(2,2);
            //triangle.Points = triangleTransformator.Mirror();

            Console.Read();
            #region comment last polygon
            //Polygon poly = PolyCanvas;
            //poly.StrokeThickness = 2;
            //poly.Stroke = Brushes.Black;
            //PointCollection points = new PointCollection();
            //points.Add(new Point(100, 100));
            //points.Add(new Point(0, 0));
            //points.Add(new Point(0, -50));


            ////poly.Points = points;
            //#region metricks
            //List<int> weights= new List<int>();
            //List<int> marks = new List<int>();
            //weights.Add(8);
            //weights.Add(4);
            //weights.Add(10);
            //weights.Add(6);
            //weights.Add(4);
            //weights.Add(9);
            //weights.Add(9);
            //weights.Add(5);
            //weights.Add(8);
            //weights.Add(7);


            //marks.Add(10);
            //marks.Add(9);
            //marks.Add(9);
            //marks.Add(6);
            //marks.Add(7);
            //marks.Add(9);
            //marks.Add(10);
            //marks.Add(6);
            //marks.Add(9);
            //marks.Add(6);
            //#endregion

            //var polyVertex = new PolygonVertex(marks, weights, 0.7);
            //poly.Points = polyVertex.GetPoints();
            ////poly.Fill = Brushes.Yellow;

            //var elps = EllipseBG;
            //elps.Width = 100;
            //elps.Height = 100;

            //double square = polyVertex.GetSumSquare();
            #endregion
        }
    }
}
