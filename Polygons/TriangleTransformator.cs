﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Polygons
{
    public class TriangleTransformator
    {
        readonly Polygon originalTriangle;
        public TriangleTransformator(Polygon triangle)
        {
            originalTriangle = triangle;
        }

        private Point VericalMatrixToPoint(double [,] matrix)
        {
            return new Point(matrix[0,0], matrix[1,0]);
        }
        private Point HorizontalMatrixToPoint(double[,] matrix)
        {
            return new Point(matrix[0, 0], matrix[0, 1]);
        }
        public PointCollection MoveTriangle(double x, double y)
        {
            var newVertex = new PointCollection();
            foreach (Point vertex in originalTriangle.Points)
            {
                newVertex.Add(VericalMatrixToPoint(Transformator.MoveToVector(vertex, x, y)));
            }
            return newVertex;
        }
        public PointCollection ScaleTriangle(double kx, double ky)
        {
            var newVertex = new PointCollection();
            foreach (Point vertex in originalTriangle.Points)
            {
                newVertex.Add(VericalMatrixToPoint(Transformator.ScaleTo(vertex, kx, ky)));
            }
            return newVertex;
        }
        public PointCollection Mirror()
        {
            var newVertex = new PointCollection();
            foreach (Point vertex in originalTriangle.Points)
            {
                newVertex.Add(VericalMatrixToPoint(Transformator.MirrorXY(vertex)));
            }
            return newVertex;
        }
    }
}
