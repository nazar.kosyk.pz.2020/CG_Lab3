﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace CG_Lab3.Converters
{
    public class CustomColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (value == null)
            {
                return null;
            }
            return value;

            //string parameterStr = value.ToString();
            //return (Color)ColorConverter.ConvertFromString(parameterStr);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            string parameterStr = value.ToString();
            return (Color)ColorConverter.ConvertFromString(parameterStr);
        }
    }
}
