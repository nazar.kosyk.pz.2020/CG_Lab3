﻿using CG_Lab3.ViewModels;
using Polygons;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace CG_Lab3.Pages
{
    /// <summary>
    /// Interaction logic for Movement.xaml
    /// </summary>
    public partial class Movement : UserControl
    {
        private double _step;
        private TriangleTransformator triangleTransformator;
        public Movement()
        {
            InitializeComponent();

            #region InitElipse
            var ellipse = new Ellipse();
            var diametr = 10;
            ellipse.Height = diametr;
            ellipse.Width = diametr;
            ellipse.Fill = Brushes.Red;
            ellipse.Stroke = Brushes.Black;
            ellipse.StrokeThickness = 1;
            PlotCanvas.Children.Add(ellipse);
            Canvas.SetZIndex(ellipse, 4);
            Canvas.SetTop(ellipse, PlotCanvas.Height / 2 - diametr / 2);
            Canvas.SetLeft(ellipse, PlotCanvas.Width / 2 - diametr / 2);
            #endregion

            #region Init X & Y lines
            var lineX = new Line();
            lineX.X1 = 0;
            lineX.Y1 = 0;
            lineX.X2 = PlotCanvas.Width;
            lineX.Y2 = 0;
            lineX.StrokeThickness = 2;
            lineX.Stroke = Brushes.Black;
            PlotCanvas.Children.Add(lineX);
            Canvas.SetZIndex(lineX, 3);
            Canvas.SetTop(lineX, PlotCanvas.Height / 2);

            var lineY = new Line();
            lineY.X1 = 0;
            lineY.Y1 = 0;
            lineY.X2 = 0;
            lineY.Y2 = PlotCanvas.Height;
            lineY.StrokeThickness = 2;
            lineY.Stroke = Brushes.Black;
            PlotCanvas.Children.Add(lineY);
            Canvas.SetZIndex(lineY, 3);
            Canvas.SetLeft(lineY, PlotCanvas.Width / 2);
            #endregion

            #region Init other lines
            var gradation = 15;
            _step = PlotCanvas.Width / (2 * gradation);
            for (int i = 1; i < gradation * 2; i++)
            {
                var lineVertical = new Line();
                lineVertical.X1 = _step * i;
                lineVertical.Y1 = 0;
                lineVertical.X2 = _step * i;
                lineVertical.Y2 = PlotCanvas.Height;
                lineVertical.StrokeThickness = 0.5;
                lineVertical.Stroke = Brushes.Gray;
                PlotCanvas.Children.Add(lineVertical);
                Canvas.SetZIndex(lineVertical, 3);

                var lineHorisontal = new Line();
                lineHorisontal.X1 = 0;
                lineHorisontal.Y1 = _step * i;
                lineHorisontal.X2 = PlotCanvas.Height;
                lineHorisontal.Y2 = _step * i;
                lineHorisontal.StrokeThickness = 0.5;
                lineHorisontal.Stroke = Brushes.Gray;
                PlotCanvas.Children.Add(lineHorisontal);
                Canvas.SetZIndex(lineHorisontal, 3);
            }
            #endregion

            var triangle = TrianglePolygon;
            triangleTransformator = new TriangleTransformator(TrianglePolygon);

            triangle.Points.Add(new Point(_step * double.Parse(x1Text.Text), _step * -double.Parse(y1Text.Text)));
            triangle.Points.Add(new Point(_step * double.Parse(x2Text.Text), _step * -double.Parse(y2Text.Text)));
            triangle.Points.Add(new Point(_step * double.Parse(x3Text.Text), _step * -double.Parse(y3Text.Text)));

            triangle.Fill = Brushes.Yellow;
            triangle.Stroke = Brushes.Blue;
            triangle.StrokeThickness = 1;
            Canvas.SetZIndex(triangle, 5);
            Canvas.SetTop(triangle, PlotCanvas.Height / 2);
            Canvas.SetLeft(triangle, PlotCanvas.Width / 2);
        }

        public void UpdateTriangle(object sender, RoutedEventArgs e)
        {
            PlotCanvas.Children.RemoveRange(1, PlotCanvas.Children.Count);

            #region InitElipse
            var ellipse = new Ellipse();
            var diametr = 10;
            ellipse.Height = diametr;
            ellipse.Width = diametr;
            ellipse.Fill = Brushes.Red;
            ellipse.Stroke = Brushes.Black;
            ellipse.StrokeThickness = 1;
            PlotCanvas.Children.Add(ellipse);
            Canvas.SetZIndex(ellipse, 4);
            Canvas.SetTop(ellipse, PlotCanvas.Height / 2 - diametr / 2);
            Canvas.SetLeft(ellipse, PlotCanvas.Width / 2 - diametr / 2);
            #endregion

            #region Init X & Y lines
            var lineX = new Line();
            lineX.X1 = 0;
            lineX.Y1 = 0;
            lineX.X2 = PlotCanvas.Width;
            lineX.Y2 = 0;
            lineX.StrokeThickness = 2;
            lineX.Stroke = Brushes.Black;
            PlotCanvas.Children.Add(lineX);
            Canvas.SetZIndex(lineX, 3);
            Canvas.SetTop(lineX, PlotCanvas.Height / 2);

            var lineY = new Line();
            lineY.X1 = 0;
            lineY.Y1 = 0;
            lineY.X2 = 0;
            lineY.Y2 = PlotCanvas.Height;
            lineY.StrokeThickness = 2;
            lineY.Stroke = Brushes.Black;
            PlotCanvas.Children.Add(lineY);
            Canvas.SetZIndex(lineY, 3);
            Canvas.SetLeft(lineY, PlotCanvas.Width / 2);
            #endregion

            #region Init other lines
            var gradation = (int)GradationSlider.Value;
            _step = PlotCanvas.Width / (2 * gradation);
            for (int i = 1; i < gradation * 2; i++)
            {
                var lineVertical = new Line();
                lineVertical.X1 = _step * i;
                lineVertical.Y1 = 0;
                lineVertical.X2 = _step * i;
                lineVertical.Y2 = PlotCanvas.Height;
                lineVertical.StrokeThickness = 0.5;
                lineVertical.Stroke = Brushes.Gray;
                PlotCanvas.Children.Add(lineVertical);
                Canvas.SetZIndex(lineVertical, 3);

                var lineHorisontal = new Line();
                lineHorisontal.X1 = 0;
                lineHorisontal.Y1 = _step * i;
                lineHorisontal.X2 = PlotCanvas.Height;
                lineHorisontal.Y2 = _step * i;
                lineHorisontal.StrokeThickness = 0.5;
                lineHorisontal.Stroke = Brushes.Gray;
                PlotCanvas.Children.Add(lineHorisontal);
                Canvas.SetZIndex(lineHorisontal, 3);
            }
            #endregion

            var triangle = TrianglePolygon;
            var viewModel = ((DataViewModel)DataContext)?.MovementViewModel;


            triangle.Points.Clear();
            triangle.Points.Add(new Point(_step * double.Parse(x1Text.Text), _step * -double.Parse(y1Text.Text)));
            triangle.Points.Add(new Point(_step * double.Parse(x2Text.Text), _step * -double.Parse(y2Text.Text)));
            triangle.Points.Add(new Point(_step * double.Parse(x3Text.Text), _step * -double.Parse(y3Text.Text)));

            triangle.Fill = Brushes.Yellow;
            triangle.Stroke = Brushes.Blue;
            triangle.StrokeThickness = 1;
            Canvas.SetZIndex(triangle, 5);
            Canvas.SetTop(triangle, PlotCanvas.Height / 2);
            Canvas.SetLeft(triangle, PlotCanvas.Width / 2);


            triangle.Points = triangleTransformator.MoveTriangle(_step * viewModel.Move, _step * -viewModel.Move);
            triangle.Points = triangleTransformator.ScaleTriangle(viewModel.ScaleX, viewModel.ScaleY);
        }

        private void Mirror(object sender, RoutedEventArgs e)
        {
            var triangle = TrianglePolygon;

            triangle.Points = triangleTransformator.Mirror();
        }
    }
}
