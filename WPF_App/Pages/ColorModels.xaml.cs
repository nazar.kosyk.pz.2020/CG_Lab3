﻿using CG_Lab3.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CG_Lab3.Pages
{
    /// <summary>
    /// Interaction logic for ColorModels.xaml
    /// </summary>
    public partial class ColorModels : UserControl
    {
        public ColorModels()
        {
            InitializeComponent();
        }
    }
}
