﻿using CG_Lab2_UI.Base.Utils;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace CG_Lab3.ViewModels
{
    public class MovementViewModel : ViewModelBase
    {
        private double _scaleX;
        private double _scaleY;
        private double _move;
        private double _gradation;
        private bool _mirror = false;
        private ICommand _updateTriangleCommand;

        public MovementViewModel()
        {
            ScaleX = 1;
            ScaleY = 1;

            Gradation = 15;
        }

        public double Move
        {
            get { return _move; }
            set
            {
                _move = value;
                OnPropertyChanged(nameof(Move));
            }
        }


        public double ScaleX { get => _scaleX; set => _scaleX = value; }
        public double ScaleY { get => _scaleY; set => _scaleY = value; }
        public bool Mirror
        {
            get { return _mirror; }
            set
            {
                _mirror = value;
                OnPropertyChanged(nameof(Mirror));
            }
        }

        public ICommand UpdateTriangleCommand
        {
            get
            {
                return _updateTriangleCommand ?? (_updateTriangleCommand = new CommandHandler(UpdateTriangle));
            }
        }

        public double Gradation { get => _gradation; set => _gradation = value; }

        private void UpdateTriangle(object parameter)
        {
        }
    }

    public class MyPoint : ViewModelBase
    {
        private double x;
        private double y;

        public double X
        {
            get => x;
            set
            {
                x = value;
                OnPropertyChanged(nameof(X));
            }
        }

        public double Y
        {
            get => y;
            set
            {
                y = value;
                OnPropertyChanged(nameof(Y));
            }
        }

        public Point ToPoint()
        {
            return new Point(X, Y);
        }

    }
}
