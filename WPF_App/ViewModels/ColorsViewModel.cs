﻿using CG_Lab3.Base.Utils;
using CG_Lab3.Commands;
using JuliaSet;
using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using MessageBox = System.Windows.Forms.MessageBox;
using OpenFileDialog = System.Windows.Forms.OpenFileDialog;

namespace CG_Lab3.ViewModels
{
    public class ColorsViewModel : ViewModelBase
    {
        private string _filePath;
        private int _saturation;
        private int _value;
        private int _halfOfImage = 0;

        private BitmapImage _image;
        private BitmapImage _originalImage;

        private ColorConvertor _colorConvertor = new ColorConvertor();

        #region Props
        public string FilePath
        {
            get { return _filePath; }
            set { _filePath = value; OnPropertyChanged(nameof(FilePath)); }
        }

        public int Saturation
        {
            get { return _saturation; }
            set { _saturation = value; OnPropertyChanged(nameof(Saturation)); }
        }

        public int Value
        {
            get { return _value; }
            set { _value = value; OnPropertyChanged(nameof(Value)); }
        }
        public int HalfOfImage
        {
            get { return _halfOfImage; }
            set { _halfOfImage = value; OnPropertyChanged(nameof(HalfOfImage)); }
        }

        public BitmapImage OriginalImage
        {
            get { return _originalImage; }
            set { _originalImage = value; OnPropertyChanged(nameof(OriginalImage)); }
        }

        public BitmapImage Image
        {
            get { return _image; }
            set { _image = value; OnPropertyChanged(nameof(Image)); }
        }

        public ICommand BrowseImageCommand { get; set; }
        public ICommand ChangeImageCommand { get; set; }
        #endregion

        public ColorsViewModel()
        {
            this.PropertyChanged += OnPropertiesChanged;

            BrowseImageCommand = new Command(OpenFile);
            ChangeImageCommand = new Command(ChangeImage);
        }

        private void OnPropertiesChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Saturation):
                    // Do something
                    break;
            }
        }

        private void OpenFile(object args)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = "D:\\kosik\\Downloads";
            openFileDialog.Filter = "txt files (*.png)|*.jpg|All files (*.*)|*.*";
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;
            
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                FilePath = openFileDialog.FileName;
            }

            if (FilePath == null)
            {
                return;
            }

            Image = OriginalImage = new BitmapImage(new Uri(FilePath));
        }

        //todo відобразити координати точок в кожній моделі кольорів відповідними значеннями    
        private void ChangeImage(object args)
        {
            var bitmapToChange = BitmapConverter.BitmapImage2Bitmap(OriginalImage);

            var halfOfImage = HalfOfImage == 1;
            var changedBitmap = _colorConvertor.ChangeSV(bitmapToChange, Saturation, Value, halfOfImage);
            Image = BitmapConverter.Bitmap2BitmapImage(changedBitmap);
        }
    }
}
