﻿using AsyncAwaitBestPractices.MVVM;
using CG_Lab3.Base.Utils;
using CG_Lab3.Commands;
using MandelbrotSet;
using System;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace CG_Lab3.ViewModels
{
    public class DataViewModel : ViewModelBase
    {
        private string _visibilityControl = "Tutorial";

        private FractalViewModel _fractalViewModel;

        private ColorsViewModel _colorsViewModel;
        private MovementViewModel _movementViewModel;

        private Mandelbrot _mandelbrot = new Mandelbrot();

        private BitmapImage _fractalImage;

        public DataViewModel()
        {
            SetVisibilityCommand = new Command(SetVisibility);
            BuildFractalCommand = new AsyncCommand(BuildFractal);

            //string gesturefile = "\\Images\\1.png";
            //_fractalImage = new BitmapImage(new Uri(gesturefile));
        }

        public string VisibilityControl
        {
            get { return _visibilityControl; }
            set { _visibilityControl = value; OnPropertyChanged(nameof(VisibilityControl)); }
        }

        public FractalViewModel FractalViewModel
        {
            get { return _fractalViewModel; }
            set { _fractalViewModel = value; OnPropertyChanged(nameof(FractalViewModel)); }
        }

        public ColorsViewModel ColorsViewModel
        {
            get { return _colorsViewModel; }
            set { _colorsViewModel = value; OnPropertyChanged(nameof(ColorsViewModel)); }
        }

        public MovementViewModel MovementViewModel
        {
            get { return _movementViewModel; }
            set { _movementViewModel = value; OnPropertyChanged(nameof(MovementViewModel)); }
        }

        public BitmapImage FractalImage
        {
            get { return _fractalImage; }
            set { _fractalImage = value; OnPropertyChanged(nameof(FractalImage)); }
        }

        public ICommand SetVisibilityCommand { get; set; }
        public IAsyncCommand BuildFractalCommand { get; set; }

        public void SetVisibility(object args)
        {
            VisibilityControl = args.ToString();
        }

        public async Task BuildFractal()
        {
            FractalImage = new BitmapImage(new Uri("/Images/loading.gif", UriKind.Relative));

            await Task.Delay(1000);

            Bitmap bitmap = null;

            switch (FractalViewModel.FractalType)
            {
                case Models.FractalType.CPlusCosZ:
                    FractalViewModel.EndColor = Color.Red;
                    bitmap = await _mandelbrot.BuildCosZc(_fractalViewModel.Width,
                    _fractalViewModel.Height, FractalViewModel.Zoom, FractalViewModel.MaxIterations,
                    FractalViewModel.StartColor, FractalViewModel.EndColor);
                    break;
                case Models.FractalType.CPlusSinZ:
                    FractalViewModel.EndColor = Color.Aqua;
                    bitmap = await _mandelbrot.BuildSinZc(_fractalViewModel.Width,
                    _fractalViewModel.Height, FractalViewModel.Zoom, FractalViewModel.MaxIterations,
                    FractalViewModel.StartColor, FractalViewModel.EndColor);
                    break;
                case Models.FractalType.CMultSinZ:
                    FractalViewModel.EndColor = Color.Green;
                    bitmap = await _mandelbrot.BuildСSinZ(_fractalViewModel.Width,
                    _fractalViewModel.Height, FractalViewModel.Zoom, FractalViewModel.MaxIterations,
                    FractalViewModel.StartColor, FractalViewModel.EndColor);
                    break;
                default:
                    break;
            }

            //bitmap.GetPixel
            FractalImage = BitmapConverter.Bitmap2BitmapImage(bitmap);
        }
    }
}
