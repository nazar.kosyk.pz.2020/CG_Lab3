﻿using CG_Lab3.Models;
using System.Drawing;

namespace CG_Lab3.ViewModels
{
    public class FractalViewModel : ViewModelBase
    {
        private FractalType fractalType;
        private int width;
        private int height;
        private int zoom;
        private int maxIterations;

        private Color startColor;
        private Color endColor;

        public FractalType FractalType
        {
            get { return fractalType; }
            set
            {
                fractalType = value;
                OnPropertyChanged(nameof(FractalType));
            }
        }
        public int Width
        {
            get { return width; }
            set { width = value; OnPropertyChanged(nameof(Width)); }
        }
        public int Height
        {
            get { return height; }
            set { height = value; OnPropertyChanged(nameof(Height)); }
        }
        public int Zoom 
        {
            get { return zoom; }
            set { zoom = value; OnPropertyChanged(nameof(Zoom)); }
        }
        public int MaxIterations 
        {
            get { return maxIterations; }
            set { maxIterations = value; OnPropertyChanged(nameof(MaxIterations)); }
        }
        public Color StartColor
        {
            get { return startColor; }
            set { startColor = value; OnPropertyChanged(nameof(StartColor)); }
        }

        public Color EndColor
        {
            get { return endColor; }
            set { endColor = value; OnPropertyChanged(nameof(EndColor)); }
        }
    }
}
