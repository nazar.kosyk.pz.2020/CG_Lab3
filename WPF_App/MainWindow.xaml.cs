﻿using AutoMapper;
using CG_Lab3.Base;
using CG_Lab3.Models;
using CG_Lab3.ViewModels;
using System.Windows;

namespace CG_Lab3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DataModel dataModel;
        private DataViewModel dataViewModel;

        public MainWindow()
        {
            new Mapping().Create();
            dataModel = new DataModel();
            //dataViewModel = Mapper.Map<DataModel, DataViewModel>(dataModel);

            dataViewModel = new DataViewModel
            {
                FractalViewModel = Mapper.Map<FractalModel, FractalViewModel>(dataModel.FractalModel),
                ColorsViewModel = new ColorsViewModel(),
                MovementViewModel = new MovementViewModel()
            };
            DataContext = dataViewModel;

            InitializeComponent();
        }
    }
}
