﻿using AutoMapper;
using CG_Lab3.Models;
using CG_Lab3.ViewModels;

namespace CG_Lab3.Base
{
    public class Mapping
    {
        public void Create()
        {
            Mapper.CreateMap<DataModel, DataViewModel>();
            Mapper.CreateMap<DataViewModel, DataModel>();

            Mapper.CreateMap<FractalModel, FractalViewModel>();
            Mapper.CreateMap<FractalViewModel, FractalModel>();
        }
    }
}
