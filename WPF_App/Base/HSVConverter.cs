﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CG_Lab3.Base
{
    public class HSVConverter
    {
        public void Convert(int sNew, int vNew, Color pixel)
        {
            int h = CountMax(pixel), s, v = 0;
        }

        //private int CountH(Color color) 
        //{

        //}

        private int CountS(Color color)
        {
            int max = CountMax(color);
            if (max == 0) return 0;

            return 1 - (max - CountMin(color));
        }

        private int CountMax(Color color)
        {
            return new List<int>() { color.R, color.G, color.B }.Max();
        }

        private int CountMin(Color color)
        {
            return new List<int>() { color.R, color.G, color.B }.Min();
        }
    }
}
