﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CG_Lab3.Models
{
    public enum FractalType
    {
        CPlusCosZ,
        CPlusSinZ,
        CMultSinZ,
    }

    public class FractalModel
    {
        public FractalType FractalType { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Zoom { get; set; }
        public int MaxIterations { get; set; }
        public Color StartColor { get; set; }
        public Color EndColor { get; set; }
    }
}
