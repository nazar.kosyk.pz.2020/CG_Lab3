﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CG_Lab3.Models
{
    public class DataModel
    {
        public FractalModel FractalModel { get; set; }

        public DataModel()
        {
            FractalModel = new FractalModel()
            {
                FractalType = FractalType.CMultSinZ,
                Height = 350,
                Width = 350,
                MaxIterations = 8,
                Zoom = 1,
                StartColor = Color.Black,
                EndColor = Color.Green
            };
        }
    }
}
