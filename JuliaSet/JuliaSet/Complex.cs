﻿using System;

namespace MandelbrotSet
{
    class Complex
    {
        private double a;
        private double b;

        public Complex(double real, double imaginative)
        {
            a = real;
            b = imaginative;
        }

        public double getModule()
        {
            return Math.Sqrt((a * a) + (b * b));
        }

        //          FORMULAS
        public Complex sinus()
        {
            var tempa = (Math.Sin(a) * Math.Cosh(b));
            var tempb = (Math.Cos(a) * Math.Sinh(b));
            //a = tempa;
            //b = tempb;
            return new Complex(tempa, tempb);
        }
        public Complex cosinus()
        {
            var tempa = (Math.Cos(a) * Math.Cosh(b));
            var tempb = -(Math.Sin(a) * Math.Sinh(b));

            //var tempa = (Math.Cos(a) * Math.Sinh(b));
            //var tempb = (Math.Sin(a) * Math.Cosh(b));
            //a = tempa;
            //b = tempb;
            return new Complex(tempa, tempb);
        }

        public void square()
        {
            var tempa = a * a - b * b;
            var tempb = 2.0 * a * b;
            a = tempa;
            b = tempb;
        }

        public void add(Complex z2Complex)
        {
            var tempa = a + z2Complex.GetA();
            var tempb = b + z2Complex.GetB();
            a = tempa;
            b = tempb;
        }

        public void multiply(Complex multiplier)
        {
            var tempa = a *multiplier.GetA() - b* multiplier.GetB();
            var tempb = a* multiplier.GetB() + b*multiplier.GetA();
            a = tempa;
            b = tempb;
        }
        
        
        //      OVERRIDES
        public static Complex operator *(Complex c1, Complex c2)
        {
            var tempa = c1.GetA() * c2.GetA() - c1.GetB() * c2.GetB();
            var tempb = c1.GetA() * c2.GetB() + c1.GetB() * c2.GetA();
            return new Complex(tempa, tempb);
        }
        public static Complex operator +(Complex c1, Complex c2)
        {
            var tempa = c1.GetA() + c2.GetA();
            var tempb = c1.GetB() + c2.GetB();
            return new Complex(tempa, tempb);
        }


        public double GetA()
        {
            return a;
        }
        public double GetB()
        {
            return b;
        }
    }
}
