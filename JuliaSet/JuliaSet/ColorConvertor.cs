﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace JuliaSet
{
    public class ColorConvertor
    {
        public List<double> RGBtoHSV(Color color)
        {
            double hue = 0, saturation = 0, value = 0;

            int R = color.R, G = color.G, B = color.B;

            int Max = Math.Max(R, Math.Max(G, B));
            int Min = Math.Min(R, Math.Min(G, B));

            if (Max != Min)
            {
                if (Max == R)
                {
                    if (G >= B)
                    {
                        hue = 60 * (G - B) / (Max - Min) + 0;
                    }
                    else // G < B
                    {
                        hue = 60 * (G - B) / (Max - Min) + 360;
                    }
                }
                else if (Max == G)
                {
                    hue = 60 * (B - R) / (Max - Min) + 120;
                }
                else if (Max == B)
                {
                    hue = 60 * (R - G) / (Max - Min) + 240;
                }
            }
            if (Max == 0)
            {
                saturation = 0;// [0]
            }
            else
            {
                saturation = 1 - (double)(Min) / Max;// [0-1]
            }
            value = (double)(Max) / 255; // [0-1]

            var HSV = new List<double>();
            HSV.Add(hue);
            HSV.Add(saturation);
            HSV.Add(value);
            return HSV;
        }

        public Color HSVtoRGB(List<double> HSV)
        {
            double hue = HSV[0], saturation = HSV[1], value = HSV[2];

            double HIndex = Math.Floor(hue / 60);

            double f = (hue / 60) - HIndex;
            double p = value * (1 - saturation);
            double q = value * (1 - saturation * f);
            double t = value * (1 - saturation * (1 - f));

            double R, G, B;
            switch (HIndex)
            {
                case 0:
                    {
                        R = value;
                        G = t;
                        B = p;
                        break;
                    }
                case 1:
                    {
                        R = q;
                        G = value;
                        B = p;
                        break;
                    }
                case 2:
                    {
                        R = p;
                        G = value;
                        B = t;
                        break;
                    }
                case 3:
                    {
                        R = p;
                        G = q;
                        B = value;
                        break;
                    }
                case 4:
                    {
                        R = t;
                        G = p;
                        B = value;
                        break;
                    }
                default://5
                    {
                        R = value;
                        G = p;
                        B = q;
                        break;
                    }
            }
            int Ri = (int)Math.Round(R * 255);
            int Gi = (int)Math.Round(G * 255);
            int Bi = (int)Math.Round(B * 255);
            var color = Color.FromArgb(Ri, Gi, Bi);
            return color;
        }

        private List<double> AddSaturation(List<double> HSV, int S)
        {
            double koef = 1 + (double)S / 100;
            double koefRed = HueKoefRED(HSV);
            if (koef == 1)
            {
                koefRed = 1;
            }

            HSV[1] *= koef * koefRed;
            if (HSV[1] > 1) HSV[1] = 1;
            return null;
        }


        private List<double> AddValue(List<double> HSV, int V)
        {
            double koef = 1 + (double)V / 100;

            double koefRed = HueKoefRED(HSV);
            if (koef == 1)
            {
                koefRed = 1;
            }

            HSV[2] *= koef;

            if (HSV[2] > 1) HSV[2] = 1;
            //var saturation = HSV[1] +(double)S/100;
            return null;
        }

        private double HueKoefRED(List<double> HSV)
        {
            var hue = HSV[0];
            double dif = 60;
            if (hue >= 300) dif = 360 - hue;
            else if (hue <= 60) dif = hue;

            return 1 - dif / 60;
        }

        //public Bitmap ChangeSV(Bitmap bitmap, int S, int V)
        //{
        //    Bitmap resultBitmap = bitmap;
        //    int corelation = 60;
        //    for (int i = 0; i < bitmap.Width; i++)
        //    {
        //        for (int j = 0; j < bitmap.Height; j++)
        //        {
        //            Color pixel = bitmap.GetPixel(i, j);
        //            var HSV = RGBtoHSV(pixel);
        //            if (HSV[0] <= 0 + corelation || HSV[0] >= 360 - corelation)
        //            //if (HSV[0] == 0)
        //            {
        //                AddSaturation(HSV, S);
        //                AddValue(HSV, V);
        //                Color newColor = HSVtoRGB(HSV);
        //                resultBitmap.SetPixel(i, j, newColor);
        //            }
        //        }
        //    }
        //    return resultBitmap;
        //}

        public Bitmap ChangeSV(Bitmap bitmap, int S, int V, bool changeHalf)
        {
            Bitmap resultBitmap = bitmap;
            int corelation = 60;
            for (int i = 0; i < bitmap.Width; i++)
            {
                if(changeHalf && i > bitmap.Width / 2)
                {
                    break;
                }

                for (int j = 0; j < bitmap.Height; j++)
                {
                    Color pixel = bitmap.GetPixel(i, j);
                    var HSV = RGBtoHSV(pixel);
                    if (HSV[0] <= 0 + corelation || HSV[0] >= 360 - corelation)
                    {
                        AddSaturation(HSV, S);
                        AddValue(HSV, V);
                        Color newColor = HSVtoRGB(HSV);
                        resultBitmap.SetPixel(i, j, newColor);
                    }
                }
            }
            return resultBitmap;
        }
    }
}
