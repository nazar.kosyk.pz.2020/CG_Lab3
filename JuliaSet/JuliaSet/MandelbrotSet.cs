﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace MandelbrotSet
{
    public class Mandelbrot
    {
        private List<double> LinespaceDoubles(double start, double end, int number)
        {
            List<double> resultList = new List<double>();
            for (int i = 0; i < number; i++)
            {
                double item = start + i*(end - start) / number;
                resultList.Add(item);
            }
            return resultList;
        }

        public async Task<Bitmap> BuildSinZc(int w, int h, int zoom, int maxIter, Color startColor, Color endColor) //lime
        {
            //const int w = 800;//columns
            //const int h = 800;//rows
            //const int zoom = 1;
            //const int maxIter = 20;

            const int moveX = 0;
            const int moveY = 0;
            const double cX = -0.7;
            const double cY = 0.27015;

            const int border = 5;


            var colors = (from c in Enumerable.Range(0, 256)
                select Color.FromArgb((c >> 5) * 36, (c >> 3 & 7) * 36, (c & 3) * 85)).ToArray();

            var colorPalette = CreateColorPalette(startColor, endColor, maxIter);
            var bitmap = new Bitmap(w, h);

            int bZoom = border / zoom;

            List<double> xList = LinespaceDoubles(-bZoom, bZoom, h);
            List<double> yList = LinespaceDoubles(-bZoom, bZoom, w);

            foreach (var xIt in xList.Select((Value, Index) => new { Value, Index }))
            {
                foreach (var yIt in yList.Select((Value, Index) => new { Value, Index }))
                {
                    int iColor = Sin(xIt.Value, yIt.Value, maxIter);

                    bitmap.SetPixel(xIt.Index, yIt.Index, colorPalette[iColor]);
                }
            }
            return bitmap; //.Save("c + sin(z).png")
        }

        public int Sin(double a, double b, int maxiter)
        {
            var c = new Complex(a, b);
            var z = new Complex(0, 0);

            for (int i = 0; i < maxiter; i++)
            {
                z = z.sinus() + c;
                if(z.getModule()>10*Math.PI) return i;
            }

            return maxiter;
        }


        public async Task<Bitmap> BuildCosZc(int w, int h, int zoom, int maxIter, Color startColor, Color endColor) //red
        {
            //const int w = 800;//columns
            //const int h = 800;//rows
            //const float zoom = 1F;
            //const int maxIter = 10;
            const int moveX = 3;
            const int moveY = 0;
            const double cX = -0.7;
            const double cY = 0.27015;

            int border = 2;


            var colors = (from c in Enumerable.Range(0, 256)
                select Color.FromArgb((c >> 5) * 36, (c >> 3 & 7) * 36, (c & 3) * 85)).ToArray();

            var colorPalette = CreateColorPalette(startColor, endColor, maxIter);
                
            var bitmap = new Bitmap(w, h);

            int bZoom = (int)(border / zoom);

            List<double> xList = LinespaceDoubles(-bZoom, bZoom, h);
            List<double> yList = LinespaceDoubles(-bZoom, bZoom, w);

            foreach (var xIt in xList.Select((Value, Index) => new { Value, Index }))
            {
                foreach (var yIt in yList.Select((Value, Index) => new { Value, Index }))
                {
                    int iColor = Cos(xIt.Value, yIt.Value, maxIter);

                    bitmap.SetPixel(xIt.Index, yIt.Index, colorPalette[iColor]);
                }
            }
            return bitmap; //.Save("c + cos(z).png");
        }

        public int Cos(double a, double b, int maxiter)
        {
            var c = new Complex(a, b);
            var z = new Complex(0,0);

            for (int i = 0; i < maxiter; i++)
            {
                z = z.cosinus()* c;
                if (Math.Abs( z.GetB() )> 50) return i;
            }

            return maxiter;
        }


        public async Task<Bitmap> BuildСSinZ(int w, int h, int zoom, int maxIter, Color startColor, Color endColor) //aqua
        {
            //const int w = 800;//columns
            //const int h = 800;//rows
            //const double zoom = 1;
            //const int maxIter = 10;
            const int moveX = 0;
            const int moveY = 0;
            const double cX = -0.7;
            const double cY = 0.27015;

            const double border = 2;


            var colors = (from c in Enumerable.Range(0, 256)
                select Color.FromArgb((c >> 5) * 36, (c >> 3 & 7) * 36, (c & 3) * 85)).ToArray();

            var colorPalette = CreateColorPalette(startColor, endColor, maxIter);

            var bitmap = new Bitmap(w, h);

            int bZoom = (int)(border / zoom);

            List<double> xList = LinespaceDoubles(-bZoom, bZoom, h);
            List<double> yList = LinespaceDoubles(-bZoom, bZoom, w);

            foreach (var xIt in xList.Select((Value, Index) => new { Value, Index }))
            {
                foreach (var yIt in yList.Select((Value, Index) => new { Value, Index }))
                {
                    int iColor = CSinZ(xIt.Value, yIt.Value, maxIter);
                    bitmap.SetPixel(xIt.Index, yIt.Index, colorPalette[iColor]);
                }
            }
            return bitmap; //.Save("c sin(z).png");
        }

        public int CSinZ(double a, double b, int maxiter)
        {
            var c = new Complex(a, b);
            var z = new Complex(Math.PI/2, Math.PI/2);

            for (int i = 0; i < maxiter; i++)
            {
                z = c * (z.sinus());
                if (Math.Abs(z.GetB()) > 50) return i;
            }

            return maxiter;
        }

        public List<Color> CreateColorPalette(Color botColor, Color topColor, int iterations)
        {
            List<Color> palette = new List<Color>();

            int stepRed, stepBlue, stepGreen;

            stepRed = (topColor.R - botColor.R) / iterations;
            stepGreen = (topColor.G - botColor.G) / iterations;
            stepBlue = (topColor.B - botColor.B) / iterations;

            palette.Add(botColor);
            for (int i = 1; i < iterations; i++)
            {
                Color LastColor = palette[i - 1];
                palette.Add(Color.FromArgb(LastColor.R + stepRed, LastColor.G + stepGreen, LastColor.B + stepBlue));
            }
            palette.Add(topColor);
            return palette;

        }
    }
}
